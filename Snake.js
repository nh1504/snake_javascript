var dir;
var df = true;
var snake;
var head;
var des;
var mov;
var sc;
var score = [];
var dr = [];
var width;
var demo = false;
var pointer;

document.onkeydown = checkKey;

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function checkKey(e) {

    e = e || window.event;

    if ((e.keyCode == '87') && (head-width != snake[1]) && (head + width * (height-3) != snake[1])) {
        dir = -width;
    }
    else if ((e.keyCode == '83') && (head+width*1 != snake[1]) && (head - width * (height-3) != snake[1])) {
		dir = width*1;
    }
    else if ((e.keyCode == '65') && (head-1 != snake[1]) && (head + width*1 - 3 != snake[1])) {
       dir = -1;
    }
    else if ((e.keyCode == '68') && (head+1 != snake[1]) && (head - width*1 + 3 != snake[1])) {
       dir = 1;
    }

}

function setup(){
	
	if(df == true){
		
		height = 31;
		width = 31;
		ac = 1;
		oc = 0;
		
	}else{
		
		height = document.getElementById("height").value;
		width = document.getElementById("width").value;
		ac = document.getElementById("apples").value;
		oc = document.getElementById("obstacles").value;
		
	}
	
	var game = document.getElementById('tbl');
	while (game.firstChild) {
	game.removeChild(game.firstChild);
	}
	var id = 0;
	for (var i = 0; i < height; i++){
		
		var tr = document.createElement('tr');
		tbl.appendChild(tr);
		for ( var j = 0; j < width; j++){
		
			var td = document.createElement('td');
			td.className = "field";
			td.id = id;
			if ((i == 0) || (i == height-1)){
			td.style.height ="1px";
			td.className = "wall"}
			else{td.style.height = "10px";}
			if ((j == 0) || (j == width-1)){
			td.style.width ="1px";
			td.className = "wall"}
			else{td.style.width = "10px";}
			tr.appendChild(td);
			id++;

		}
		
	}
	document.getElementById('pref').style.height = height*10+130+"px";
	x = Math.floor(id/2);
	if(document.getElementById(x).className == "wall"){
		x+=3;
	}
	snake = [x,x+1,x+2,x+3,x+4]
	snake.forEach(element => document.getElementById(element).className = "snake");
	head = snake[0];
	sc=0;
	document.getElementById("Score").innerHTML = sc;
	for(i = 0; i < ac; i++){
		apple();
	}
	for(i = 0; i < oc; i++){
		obstacle();
	}
	
	
	
}

function dfstart(){		
	if(df == false){
		switch(document.getElementById("difficulty").value){
			case '1':speed = 150;
			break;
			case '2':speed = 100;
			break;
			case '3':speed = 50;
			break;
			}
	}else{
		speed = 100;
	}
	setup();	
	clearInterval(mov);
	dir = 0;
	mov = window.setInterval( step, speed );
	
}

function step(){
	if(dir != 0){
		des = head + dir;
		if(document.getElementById(des).className != "field"){
		collision();
		}else{
		t = snake[snake.length-1];
		document.getElementById(t).className="field";
		document.getElementById(des).className="snake";
		f = snake.pop();
		snake.unshift(des);
		head=snake[0];
		}
	}
}

function collision(){
	if((document.getElementById(des).className == "wall") || (document.getElementById(des).className == "snake") || (document.getElementById(des).className == "obstacle")){	
		if((df == false) && (document.getElementById("Cartoon").checked == true) && (document.getElementById(des).className == "wall")){
			switch(dir){
				case (width*1):
				des = head - width * (height-3);
				break;
				case -width:
				des = head + width * (height-3);
				break;
				case 1:
				des = head - width + 3;
				break;
				case -1:
				des = head + width*1 - 3;
				break;
			}
			t = snake[snake.length-1];
			document.getElementById(t).className="field";
			document.getElementById(des).className="snake";
			f = snake.pop();
			snake.unshift(des);
			head=snake[0];
		}else if((df == false) && (document.getElementById("Cut").checked == true) && (document.getElementById(des).className == "snake")){
			
			while(snake[snake.indexOf(des)] != null){
				
				t = snake[snake.length-1];
				document.getElementById(t).className="field";
				snake.pop();
				
				
			}
		
			sc=snake.length-5;
			document.getElementById("Score").innerHTML = sc;
		
		}else{
			
			clearInterval(mov);
			evaluate();
			
		}
	}
	if(document.getElementById(des).className == "apple"){
		
		document.getElementById(des).className="snake";
		snake.unshift(des);
		head=snake[0];
		sc++;
		document.getElementById("Score").innerHTML = sc;
		apple();
	}
}

function apple(){
	pa = document.getElementsByClassName("field");
	ai = Math.floor(Math.random() * (pa.length));
	pa[ai].className = "apple";
}

function obstacle(){
	
	pa = document.getElementsByClassName("field");
	ai = Math.floor(Math.random() * (pa.length));
	pa[ai].className = "obstacle";
	
}

function createScore(){
	
	var hs = document.getElementById('hs');
	for (i = 0; i < 11; i++){
		var tr = document.createElement('tr');
		hs.appendChild(tr);
		for (j = 0; j < 3; j++){
			var td = document.createElement('td');
			td.id = i+"-"+j;
			tr.appendChild(td);
		}
	}
	document.getElementById("0-0").innerHTML= "Rank";
	document.getElementById("0-1").innerHTML= "Name";
	document.getElementById("0-2").innerHTML= "Score";
	if(document.cookie == ""){
		score.push(["Dare",835]);
		score.push(["Pride",750]);
		score.push(["Envy",600]);
		score.push(["Time",450]);
		score.push(["Ambition",300]);
		score.push(["AOC",150]);
		score.push(["Self",50]);
		score.push(["Purpose",20]);
		score.push(["CHD",1]);
		score.push(["Doom",0]);
		updateScore();
	}else{
		ph = document.cookie.split(";");
		for (i = 0; i < 10; i++) {
			ph[i*2]   = ph[i*2].split("=")[1];
			ph[i*2+1] = ph[i*2+1].split("=")[1];
			score.push([ph[i*2],ph[i*2+1]]);
		}
		updateScore();
    }

}

function evaluate(){
	if(df == true){
		for(i = 0; i < 10; i++){
			if(score[i][1] < sc){
				name = prompt("You reached the #" + (i + 1) + " Score. Please enter your name.");
				f = score.pop();
				score.splice(i,0,[name,sc]);
				i = 10;
				updateScore();
			}
			if(i == 9){
				alert('You reached a Score of ' + sc+ '. You achieved nothing');
			}
		}
	}else{
		alert('You reached a Score of ' + sc+ '.');
	}
}

function updateScore(){
	deleteAllCookies();
	for(i = 0; i < 10; i++){
			id=[(i+1+"-0"),(i+1+"-1"),(i+1+"-2")];
			document.getElementById(id[0]).innerHTML=i+1;
			document.getElementById(id[1]).innerHTML=score[i][0];
			document.getElementById(id[2]).innerHTML=score[i][1];
			document.cookie = i+"name="+score[i][0];
			document.cookie = i+"score="+score[i][1];
		}
}

function Demo(){
	
	if(demo == false){
		
		demo = true;
		var game = document.getElementById('tbl');
		while (game.firstChild) {
		game.removeChild(game.firstChild);
		}
		var id = 0;
		for (var i = 0; i < 20; i++){
		
			var tr = document.createElement('tr');
			tbl.appendChild(tr);
			for ( var j = 0; j < 20; j++){
		
				var td = document.createElement('td');
				td.className = "field";
				td.id = id;
				if ((i == 0) || (i == 19)){
				td.style.height ="1px";
				td.className = "wall"}
				else{td.style.height = "10px";}
				if ((j == 0) || (j == 19)){
				td.style.width ="1px";
				td.className = "wall"}
				else{td.style.width = "10px";}
				tr.appendChild(td);
				id++;

			}
		
		}
		
		hc = [];
		for(i = 21; i < 39; i++){
			
			if(i != 38){
				hc.push(i+1);
			}else{
				hc.push(i+20);
			}
		}
		j = hc[hc.length-1];
		for(c = 0; c < 8; c++){
			for(i = j; i > j-17; i--){
				if(i != j-16){
					hc.push(i-1);
				}else{
					hc.push(i+20);
				}
			}
		j = hc[hc.length-1];
		for(i = j; i < j + 17; i++){
			if(i != j+16){
				hc.push(i+1);
			}else{
					hc.push(i+20);
				}
			}
		j = hc[hc.length-1];	
		}
		for(i = 378; i > 360; i--){
			
			if(i != 361){
				hc.push(i-1);
			}else{
				hc.push(i-20);
			}
		}
		for(i = 341; i > 21; i-=20){
			hc.push(i-20);
		}
	snake = [hc[4],hc[3],hc[2],hc[1],hc[0]]
	snake.forEach(element => document.getElementById(element).className = "snake");
	apple();
	pointer = 5;
	mov = window.setInterval( dstep, 1);
}
}
function dstep(){
	des = hc[pointer];
	if(document.getElementById(des).className != "field"){
		dcollision();
		pointer++;
		if(pointer == hc.length){
			pointer = 0;
		}
	}else{
		t = snake[snake.length-1];
		document.getElementById(t).className="field";
		document.getElementById(des).className="snake";
		snake.pop();
		snake.unshift(des);
		pointer++;
		if(pointer == hc.length){
			pointer = 0;
		}
	}
	if(document.getElementsByClassName("apple").length<1){
		apple();
	}
}

function dcollision(){
	
	if(document.getElementById(des).className == "apple"){
		document.getElementById(des).className="snake";
		snake.unshift(des);
		apple();
	}else{
		
		clearInterval(mov);
		alert("Demo end");
		demo = false;
		
	}
	
}
